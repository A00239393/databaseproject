import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

@SuppressWarnings("serial")
public class JDBCMainWindowContent extends JInternalFrame implements ActionListener
{	
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private JPanel detailsPanel;
	private JPanel exportButtonPanel;
	//private JPanel exportConceptDataPanel;
	private JScrollPane dbContentsPanel;

	private Border lineBorder;

	private JLabel IDLabel=new JLabel("Agent Code:                 ");
	private JLabel NameLabel=new JLabel("Name:               ");
	private JLabel AreaLabel=new JLabel("Working Area:      ");
	private JLabel CommissionLabel=new JLabel("Commission:        ");
	private JLabel PhoneNumberLabel=new JLabel("Phone Number:                 ");

	private JTextField IDTF= new JTextField(10);
	private JTextField NameTF=new JTextField(10);
	private JTextField AreaTF=new JTextField(10);
	private JTextField CommissionTF=new JTextField(10);
	private JTextField PhoneNumberTF=new JTextField(10);



	private static QueryTableModel TableModel = new QueryTableModel();
	//Add the models to JTabels
	private JTable TableofDBContents=new JTable(TableModel);
	//Buttons for inserting, and updating members
	//also a clear button to clear details panel
	private JButton updateButton = new JButton("Update");
	private JButton insertButton = new JButton("Insert");
	private JButton exportButton  = new JButton("Export Agents with Customers");
	private JButton deleteButton  = new JButton("Delete");
	private JButton clearButton  = new JButton("Clear");
	private JButton customerButton  = new JButton("Customers");
	private JButton orderButton  = new JButton("Orders");
	private JButton agentButton  = new JButton("Agent");

	private JButton  OrderPerCustomer = new JButton("Order Per Customer ID:");
	private JTextField OrderPerCustomerTF  = new JTextField(12);
	private JButton CustomersPerAgent  = new JButton("Customers For Agent ID");
	private JTextField CustomersPerAgentTF  = new JTextField(12);
	private JButton ListAllCustomers  = new JButton("ListAllCustomers");
	private JButton ListAllAreas  = new JButton("Count Of All Countries - Customers");
	
	private JButton  OrderPayment = new JButton("Order Payment left - Order ID:");
	private JTextField OrderPaymentTF  = new JTextField(12);



	public JDBCMainWindowContent( String aTitle)
	{	
		//setting up the GUI
		super(aTitle, false,false,false,false);
		setEnabled(true);

		initiate_db_conn();
		//add the 'main' panel to the Internal Frame
		content=getContentPane();
		content.setLayout(null);
		content.setBackground(Color.lightGray);
		lineBorder = BorderFactory.createEtchedBorder(15, Color.red, Color.black);

		//setup details panel and add the components to it
		detailsPanel=new JPanel();
		detailsPanel.setLayout(new GridLayout(11,2));
		detailsPanel.setBackground(Color.lightGray);
		detailsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "CRUD Actions - Agent Only"));

		detailsPanel.add(IDLabel);			
		detailsPanel.add(IDTF);
		detailsPanel.add(NameLabel);		
		detailsPanel.add(NameTF);
		detailsPanel.add(AreaLabel);		
		detailsPanel.add(AreaTF);
		detailsPanel.add(CommissionLabel);	
		detailsPanel.add(CommissionTF);
		detailsPanel.add(PhoneNumberLabel);		
		detailsPanel.add(PhoneNumberTF);

		//setup details panel and add the components to it
		exportButtonPanel=new JPanel();
		exportButtonPanel.setLayout(new GridLayout(4,2));
		exportButtonPanel.setBackground(Color.lightGray);
		exportButtonPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Export Data"));
		exportButtonPanel.add(OrderPerCustomer);
		exportButtonPanel.add(OrderPerCustomerTF);
		exportButtonPanel.add(CustomersPerAgent);
		exportButtonPanel.add(CustomersPerAgentTF);
		exportButtonPanel.add(OrderPayment);
		exportButtonPanel.add(OrderPaymentTF);
		exportButtonPanel.add(ListAllCustomers);
		exportButtonPanel.add(ListAllAreas);
		exportButtonPanel.setSize(500, 200);
		exportButtonPanel.setLocation(3, 300);
		content.add(exportButtonPanel);

		insertButton.setSize(100, 30);
		updateButton.setSize(100, 30);
		exportButton.setSize (300, 30);
		deleteButton.setSize (100, 30);
		clearButton.setSize (100, 30);
		customerButton.setSize (100, 30);
		orderButton.setSize (100, 30);
		agentButton.setSize (100, 30);

		insertButton.setLocation(370, 30);
		updateButton.setLocation(370, 150);
		exportButton.setLocation (850, 300);
		deleteButton.setLocation (370, 90);
		clearButton.setLocation (370, 210);
		customerButton.setLocation (550, 300);
		orderButton.setLocation (650, 300);
		agentButton.setLocation (750, 300);

		insertButton.addActionListener(this);
		updateButton.addActionListener(this);
		exportButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);
		customerButton.addActionListener(this);
		orderButton.addActionListener(this);
		agentButton.addActionListener(this);

		this.ListAllCustomers.addActionListener(this);
		this.OrderPerCustomer.addActionListener(this);
		this.CustomersPerAgent.addActionListener(this);
		this.ListAllAreas.addActionListener(this);
		this.OrderPayment.addActionListener(this);

		content.add(insertButton);
		content.add(updateButton);
		content.add(exportButton);
		content.add(deleteButton);
		content.add(clearButton);
		content.add(customerButton);
		content.add(orderButton);
		content.add(agentButton);


		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel=new JScrollPane(TableofDBContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(Color.lightGray);
		dbContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder,"Database Content"));

		detailsPanel.setSize(360, 300);
		detailsPanel.setLocation(3,0);
		dbContentsPanel.setSize(700, 300);
		dbContentsPanel.setLocation(477, 0);

		content.add(detailsPanel);
		content.add(dbContentsPanel);

		setSize(982,645);
		setVisible(true);

		TableModel.refreshFromDB(stmt);
	}

	public void initiate_db_conn()
	{
		try
		{
			
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			//String url="jdbc:mysql://localhost:3306/project4database";
			String url="jdbc:mysql://localhost:3307/project4database";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
			
		}
	}

	//event handling 
	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		if (target == clearButton)
		{
			IDTF.setText("");
			NameTF.setText("");
			AreaTF.setText("");
			CommissionTF.setText("");
			PhoneNumberTF.setText("");
			OrderPerCustomerTF.setText("");
			CustomersPerAgentTF.setText("");
			


		}

		if (target == insertButton)
		{		 
			try
			{
				String updateTemp ="call insert_agents('"+NameTF.getText()+"','"+AreaTF.getText()+"','"+CommissionTF.getText()+"','"+PhoneNumberTF.getText()+"','"+IDTF.getText() +"');";

				System.out.println(updateTemp);
				stmt.executeUpdate(updateTemp);

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  insert:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		if (target == deleteButton)
		{

			try
			{
				String updateTemp ="DELETE FROM agents WHERE agent_code = '"+IDTF.getText()+"';"; 
				System.out.println(updateTemp);
				stmt.executeUpdate(updateTemp);
				

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with delete:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		if (target == updateButton)
		{	 	
			try
			{ 			
				/*
				 * String updateTemp ="UPDATE agents SET " + "Agent_Name = '"+NameTF.getText()+
				 * "', Working_Area = '"+AreaTF.getText()+
				 * "', Commission = "+CommissionTF.getText()+
				 * ", Phone_No ='"+PhoneNumberTF.getText()+
				 * "' where agent_code = '"+IDTF.getText()+"';";
				 */

				String spUpdateAgents = "call update_agent('"+NameTF.getText()+"','"+AreaTF.getText()+"','"+CommissionTF.getText()+"','"+PhoneNumberTF.getText()+"','"+IDTF.getText()+"');";
				System.out.println(spUpdateAgents);
				stmt.executeUpdate(spUpdateAgents);
				
				//these lines do nothing but the table updates when we access the db.
				rs = stmt.executeQuery("SELECT * from agents ");
				rs.next();
				rs.close();	
			}
			catch (SQLException sqle){
				System.err.println("Error with  update:\n"+sqle.toString());
			}
			finally{
				TableModel.refreshFromDB(stmt);
			}
		}
		if (target == customerButton)
		{		 
			String table= "customer";
			TableModel.changeTable(stmt,table);
		}
		if (target == orderButton)
		{		 
			String table= "orders";
			TableModel.changeTable(stmt,table);
		}
		if (target == agentButton)
		{		 
			String table= "agents";
			TableModel.changeTable(stmt,table);
		}
		if (target == exportButton)
		{	
			
			  cmd="select agents.agent_code, agents.agent_name, customer.cust_name, customer.cust_city\r\n" + 
			  		"from customer\r\n" + 
			  		"inner join agents on customer.agent_code=agents.agent_code ;";
			  try{					
					rs= stmt.executeQuery(cmd); 	
					writeToFile(rs);
				}
				catch(Exception e1){e1.printStackTrace();}
			
		}
			
			

		/////////////////////////////////////////////////////////////////////////////////////
		//I have only added functionality of 2 of the button on the lower right of the template
		///////////////////////////////////////////////////////////////////////////////////

		if(target == this.ListAllCustomers){

			cmd = "select distinct cust_name from customer;";

			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
			}
			catch(Exception e1){e1.printStackTrace();}

		}

		if(target == this.OrderPerCustomer){
			
			String custID = this.OrderPerCustomerTF.getText();

			cmd = "select * from orders where cust_code='"+custID+"'";

			System.out.println(cmd);
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
			}
			catch(Exception e1){e1.printStackTrace();}

		} 
		
		if(target == this.CustomersPerAgent){
			String agent_code = this.CustomersPerAgentTF.getText();
			cmd = "select * from customer where agent_code='"+ agent_code +"';";
			System.out.println(cmd);
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
			}
			catch(Exception e1){e1.printStackTrace();}

		}
		
		if(target == this.ListAllAreas){
			
			//cmd = "select count(cust_country) as 'Count',cust_country from customer group by cust_country;";
			cmd="select * from allareas";
			System.out.println(cmd);
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
			}
			catch(Exception e1){e1.printStackTrace();}

		}
		
		if(target == this.OrderPayment){
			String ord_num = this.OrderPaymentTF.getText();
			cmd="select calculate_order_pay_left("+ord_num+")";
			System.out.println(cmd);
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
			}
			catch(Exception e1){e1.printStackTrace();}

		}
		
		

	}
	///////////////////////////////////////////////////////////////////////////

	private void writeToFile(ResultSet rs){
		try{
			System.out.println("In writeToFile");
			//Home pc
			//FileWriter outputFile = new FileWriter("D:\\Users\\Mark\\Documents\\BitBucket Repos\\databaseproject\\Sheila.csv");
			//Laptop
			FileWriter outputFile = new FileWriter("D:\\Mark\\Documents\\Clones\\databaseproject\\Sheila.csv");
			//School Pc
			//FileWriter outputFile = new FileWriter("C:\\Users\\a00239393\\Documents\\Repo\\databaseproject\\Sheila.csv");
			
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();
		System.out.println("Please set a valid directory for the csv file output");
		}
	}
}
